import os, sys
import numpy as np
import cv2
from random import randint
from PIL import Image
from multiprocessing import Pool
import imageio
import argparse
from IPython import embed  # to debug
import skvideo.io
import scipy.misc
from skimage import img_as_ubyte

skvideo.setFFmpegPath('C:\\ffmpeg-20190225-f948082-win64-static\\bin')
totalVideosProcessed = 0
num_frames = 25

def parse_args():
    # taken from denseFlow
    # --data_root C:\Users\David\Desktop\Licenta --dataset VIDEO_RGB
    parser = argparse.ArgumentParser(description="process data to be used for I3D training")
    parser.add_argument('--dataset', default='VIDEO_RGB', type=str, help='set the dataset name, to find the data path')
    parser.add_argument('--data_root', default='C:\\Users\\David\\Desktop\\Licenta', type=str)
    parser.add_argument('--new_dir', default='processedData', type=str)
    parser.add_argument('--num_workers', default=4, type=int, help='num of workers to act multi-process')
    parser.add_argument('--step', default=1, type=int, help='gap frames')
    parser.add_argument('--bound', default=15, type=int, help='set the maximum of optical flow')
    parser.add_argument('--s_', default=0, type=int, help='start id')
    parser.add_argument('--e_', default=1980, type=int, help='end id')  # 1980
    parser.add_argument('--mode', default='rgb', type=str,
                        help='set \'rgb\' if you want to process the rgb stream, else set \'optical_flow\'')
    args = parser.parse_args()
    return args


def get_video_list():
    # taken from denseFlow
    video_list = []
    for cls_names in os.listdir(data_root):
        cls_path = os.path.join(data_root, cls_names)
        for video_ in os.listdir(cls_path):
            video_list.append(video_)
    video_list.sort()
    # video_list.pop(0)  # hack
    return video_list, len(video_list)


def center_crop_frame(frame):
    # width and height are selected according to the papaer
    new_width = 224
    new_height = 224

    height = np.size(frame, 0)
    width = np.size(frame, 1)

    left = int(np.ceil((width - new_width) / 2.))
    top = int(np.ceil((height - new_height) / 2.))
    # right = int(np.floor((width + new_width) / 2.))
    right = int(width - np.floor((width - new_width) / 2))
    bottom = int(np.floor((height + new_height) / 2.))
    croppedFrame = frame[top:bottom, left:right]

    # croppedFrame = np.round(croppedFrame).astype(np.int8)
    return croppedFrame


def save_video(video, data_root, new_dir, save_dir, type):
    data_root = "E:\\DataProcessing"
    if type == 'RGB':
        path = os.path.join(data_root, new_dir, 'rgb', save_dir.split('_')[1])
        if not os.path.exists(path):
            os.makedirs(path)

        # video_path = os.path.join(path, save_dir + '.avi')
        # imageio.mimwrite(video_path, video, fps=19)
        np.save(os.path.join(path, save_dir), video)


def save_flow(video_flows, test_flow, data_root, new_dir, save_dir, type):
    data_root = "D:\\DataProcessing\\TestVideo"
    path = os.path.join(data_root, new_dir, 'flow', save_dir.split('_')[1])
    if not os.path.exists(path):
        os.makedirs(path)

    video_path = os.path.join(path, save_dir + '.avi')
    # trainingFlow = video_flows


    # my_glow = np.load("v_CricketShot_g04_c01_flow.npy")
    my_glow = test_flow[0]

    # Video testing start
    zerosArray = np.zeros(test_flow.shape[:-1])
    firstArray = test_flow[:, :, :, 0]
    secondArray = test_flow[:, :, :, 1]
    test_flow = np.stack((firstArray, secondArray, zerosArray), axis=-1)
    imageio.mimwrite(video_path, test_flow, fps=19)
    # Video testing end
    np.save(os.path.join(path, save_dir), video_flows)


def get_video(video_name, video_path):
    try:
        video_capture = skvideo.io.vread(video_path)
    except Exception as e:
        print(e)
        print('{} read error! ', format(video_name))
        return 0
    print(video_name)
    # if extract nothing, exit!
    if video_capture.sum() == 0:
        print('Could not initialize capturing', video_name)
        exit()
    len_frame = len(video_capture)

    return video_capture, len_frame


def compute_TVL1(prev, curr):
    """Compute the TV-L1 optical flow."""
    TVL1 = cv2.optflow.createOptFlow_DualTVL1()
    flow = TVL1.calc(prev, curr, None)
    assert flow.dtype == np.float32

    flow[flow < -20] = -20
    flow[flow > 20] = 20

    min_flow = np.min(flow)
    max_flow = np.max(flow)
    # change range to (0, 1) to have the videos viewable in a player
    flow = np.interp(flow, (min_flow, max_flow), (0, 1))

    return flow


def lengthwise_crop(video):
    if video.shape[0] > num_frames:
        start_frame = randint(0, video.shape[0] - num_frames)
        video = video[start_frame:(start_frame + num_frames), :, :, :]
        return video

    else:
        new_video = np.zeros((num_frames, 224, 224, 1)) #if rgb, change for optiflow
        new_video[0:video.shape[0], :, :, :] = video[:, :, :, :]
        new_video[(video.shape[0] + 1): num_frames, :, :, :] = video[0:(num_frames - video.shape[0])]
        return new_video


def process_rgb_video(video_capture):
    # we assume the video is in a wide-format (e.g. 16/9) such as the smallest dimension is video_capture.shape[1]
    SMALLEST_DIMENSION = 256
    aspect_ratio = video_capture.shape[2] / video_capture.shape[1]
    height = SMALLEST_DIMENSION
    width = int(aspect_ratio * height)

    resized_video = np.empty((video_capture.shape[0], 224, 224, 3))

    for i in range(0, video_capture.shape[0]):
        frame = video_capture[i]
        resized_frame = cv2.resize(frame, (width, height), interpolation=cv2.INTER_LINEAR)

        minimum_in_frame = np.amin(resized_frame)
        maximum_in_frame = np.amax(resized_frame)
        # change range to (0, 1) to have the videos viewable in a player
        resized_frame = np.interp(resized_frame, (minimum_in_frame, maximum_in_frame), (-1, +1))

        centered_frame = center_crop_frame(resized_frame)
        # optiflow[i - 1] = centered_frame
        resized_video[i] = centered_frame

    print(resized_video.shape)
    return resized_video


def process_optiflow(video_capture):
    SMALLEST_DIMENSION = 256
    aspect_ratio = video_capture.shape[2] / video_capture.shape[1]
    height = SMALLEST_DIMENSION
    width = int(aspect_ratio * height)
    # optiflow = np.empty((video_capture.shape[0], height, width, 2))
    optiflow = np.empty((video_capture.shape[0], 224, 224, 2))
    test_optiflow = np.empty((video_capture.shape[0], 224, 224, 2))

    previous_frame = video_capture[0]
    resized_previous_frame = cv2.resize(previous_frame, (width, height), interpolation=cv2.INTER_LINEAR)
    resized_previous_frame = cv2.cvtColor(resized_previous_frame, cv2.COLOR_BGR2GRAY)
    for i in range(1, video_capture.shape[0]):
        frame = video_capture[i]
        resized_frame = cv2.resize(frame, (width, height), interpolation=cv2.INTER_LINEAR)
        grayscale_frame = cv2.cvtColor(resized_frame, cv2.COLOR_BGR2GRAY)
        optiflowed_frame = compute_TVL1(resized_previous_frame, grayscale_frame)
        resized_previous_frame = grayscale_frame
        # resized_optiflowed_frame = cv2.resize(optiflowed_frame, (width, height), interpolation=cv2.INTER_LINEAR)
        # resized_optiflowed_frame = np.round(resized_optiflowed_frame).astype(np.int8)
        # optiflowed_frame = np.round(optiflowed_frame).astype(np.int8)
        # minimum_in_frame = np.amin(resized_optiflowed_frame)
        # maximum_in_frame = np.amax(resized_optiflowed_frame)
        # # change range to (0, 1) to have the videos viewable in a player
        # resized_frame = np.interp(resized_optiflowed_frame, (minimum_in_frame, maximum_in_frame), (0, 255))
        # resized_frame = resized_optiflowed_frame / 128.0 - 1
        centered_frame = center_crop_frame(optiflowed_frame)
        optiflow[i - 1] = centered_frame

        # info = np.iinfo(centered_frame.dtype)  # Get the information of the incoming image type
        data = centered_frame / np.max(centered_frame)  # normalize the data to 0 - 1
        # data = 255 * data  # Now scale by 255
        # scaledOptiflowed = data.astype(np.uint8)
        test_optiflow[i - 1] = data
        optiflow[i - 1] = data
        # test_optiflow = test_optiflow + 0.5
        # test_optiflow[i - 1] = np.dstack((resized_optiflowed_frame, np.zeros(resized_optiflowed_frame.shape[:-1])))

    global totalVideosProcessed
    totalVideosProcessed = totalVideosProcessed + 1

    print("%d/1981 videos processed" % totalVideosProcessed)
    return optiflow, test_optiflow


def rgb(augs):
    '''
       Preprocesses the rgb videos
       :param augs:the detailed augments:
           video_name: the video name which is like: 'v_xxxxxxx',if different, please have a modify.
           save_dir: the destination path's final direction name.
           bound: bi-bound parameter
       :return: no returns
    '''

    args = parse_args()
    data_root = os.path.join(args.data_root, args.dataset)
    new_dir = args.new_dir

    video_name, save_dir, bound = augs
    video_path = os.path.join(data_root, video_name.split('_')[1], video_name)

    video_capture, len_frame = get_video(video_name, video_path)
    resized_video = process_rgb_video(video_capture)
    cropped_video = lengthwise_crop(resized_video)
    save_video(cropped_video, data_root, new_dir, save_dir, 'RGB')


def dense_flow(augs):
    args = parse_args()
    data_root = os.path.join(args.data_root, args.dataset)
    new_dir = args.new_dir

    video_name, save_dir, bound = augs
    video_path = os.path.join(data_root, video_name.split('_')[1], video_name)

    video_capture, len_frame = get_video(video_name, video_path)
    resized_video, test_optiflow = process_optiflow(video_capture)
    # resized_video = np.round(resized_video).astype(np.uint8)
    # test_optiflow = np.round(test_optiflow).astype(np.uint8)
    save_flow(resized_video, test_optiflow, data_root, new_dir, save_dir, 'optiflow')


if __name__ == '__main__':
    args = parse_args()
    data_root = os.path.join(args.data_root, args.dataset)

    # specify the augments
    num_workers = args.num_workers
    step = args.step
    bound = args.bound
    s_ = args.s_
    e_ = args.e_
    new_dir = args.new_dir
    mode = args.mode
    # get video list
    video_list, len_videos = get_video_list()
    video_list = video_list[s_:e_]

    print('find {} videos.', format(len_videos))
    flows_dirs = [video.split('.')[0] for video in video_list]
    print('get videos list done!')

    pool = Pool(num_workers)
    if mode == 'rgb':
        pool.map(rgb, zip(video_list, flows_dirs, [bound] * len(video_list)))
    else:  # mode=='optical_flow'
        pool.map(dense_flow, zip(video_list, flows_dirs, [bound] * len(video_list)))
