import os
import numpy as np
from random import randint
import imageio

dirName = "E:\\DataProcessing\\processedData\\flow"
num_frames = 25
processed_videos = 0


def get_video_list():
    # taken from denseFlow
    data_root = "E:\\DataProcessing\\processedData\\flow"
    video_list = []
    for cls_names in os.listdir(data_root):
        cls_path = os.path.join(data_root, cls_names)
        for video_ in os.listdir(cls_path):
            video_list.append(video_)
    video_list.sort()
    # video_list.pop(0)  # hack
    return video_list, len(video_list)

def getListOfFiles(dirName):
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)

    return allFiles


def lengthwise_crop_existing_optiflow():
    list_of_files = getListOfFiles(dirName)

    video_list, len_videos = get_video_list()
    video_list = video_list[0:1980]
    flows_dirs = [video.split('.')[0] for video in video_list]

    i = 0
    for elemPath in list_of_files:
        optiflowed_movie = np.load(elemPath)
        cropped_movie = lengthwise_crop(optiflowed_movie)
        save_flow(cropped_movie, flows_dirs[i])
        i = i + 1


def lengthwise_crop(video):
    if video.shape[0] >= num_frames:
        start_frame = randint(0, video.shape[0] - num_frames)
        video = video[start_frame:(start_frame + num_frames), :, :, :]
        return video

    else:
        new_video = np.zeros((num_frames, 224, 224, 2)) #if rgb, change for optiflow
        new_video[0:video.shape[0], :, :, :] = video[:, :, :, :]
        new_video[(video.shape[0] + 1):num_frames, :, :, :] = video[0:(num_frames - video.shape[0])]
        return new_video


def save_flow(video_flow, save_dir):
    data_root = "E:\\DataProcessing"
    path = os.path.join(data_root, 'processedData', 'croppedFlow', save_dir.split('_')[1])
    if not os.path.exists(path):
        os.makedirs(path)

    # Video testing start
    # zerosArray = np.zeros(video_flow.shape[:-1])
    # firstArray = video_flow[:, :, :, 0]
    # secondArray = video_flow[:, :, :, 1]
    # test_flow = np.stack((firstArray, secondArray, zerosArray), axis=-1)

    # video_path = os.path.join(path, save_dir + '.avi')
    # imageio.mimwrite(video_path, test_flow, fps=19)
    # Video testing end
    np.save(os.path.join(path, save_dir), video_flow)

    global processed_videos
    processed_videos = processed_videos + 1
    print("%d/1680 videos processed" % processed_videos)


if __name__ == '__main__':
    lengthwise_crop_existing_optiflow()
